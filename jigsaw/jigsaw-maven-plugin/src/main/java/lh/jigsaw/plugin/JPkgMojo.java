package lh.jigsaw.plugin;

import java.io.File;
import java.io.IOException;
import java.lang.module.ModuleInfo;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.openjdk.jigsaw.JigsawModuleSystem;
import org.openjdk.jigsaw.cli.*;

/**
 * Package the module into a jmod file.<br>
 * Drops the file into ${project.build.directory}/module
 *
 * @requiresProject
 * @goal jpkg
 * @requiresDependencyResolution runtime
 * @phase package
 *
 * @author ludovic
 */
public class JPkgMojo extends AbstractMojo
{

  /**
   * Library directory
   *
   * @parameter default-value="${project.build.directory}/library"
   * @required
   */
  private File libraryDirectory;

  /**
   * Classes directory.
   *
   * @parameter expression="${project.build.outputDirectory}"
   * @required
   * @readonly
   */
  private File classesDir;

  /**
   * Output directory.
   *
   * @parameter expression="${project.build.directory}/module"
   * @required
   * @readonly
   */
  private File outputDir;

  /**
   * Module name
   *
   * @parameter expression="${moduleName}"
   * @readonly
   */
  private String moduleName;

  @Override
  public void execute() throws MojoExecutionException, MojoFailureException
  {
    getLog().info("JPkgMojo");
    try
    {
      if (outputDir.exists())
      {
        deleteDir(outputDir.toPath());
      }

      outputDir.mkdirs();

      String[] params = new String[]
      {
        "-L",
        libraryDirectory.getAbsolutePath(),
        "-m",
        classesDir.getAbsolutePath(),
        "-d",
        outputDir.getAbsolutePath(),
        "jmod",
        getModuleName()
      };
     
      getLog().info(Arrays.toString(params));
     
      Packager.run(params);
    }
    catch (MojoExecutionException ex)
    {
      throw ex;
    }
    catch (Exception ex)
    {
      throw new MojoExecutionException("Could not package '" + moduleName + "'.", ex);
    }

  }

  private String getModuleName() throws MojoExecutionException
  {
    if (moduleName == null)
    {
      try
      {
        Path path = classesDir.toPath().resolve("module-info.class");
        byte[] bytes = java.nio.file.Files.readAllBytes(path);
        JigsawModuleSystem jms = JigsawModuleSystem.instance();
        ModuleInfo mif = jms.parseModuleInfo(bytes);
        moduleName = mif.id().name();
        getLog().info("module name read from module-info = " + moduleName);

      }
      catch (IOException ex)
      {
        throw new MojoExecutionException("Could not read the module name, check that module-info.class exist.", ex);
      }
    }
    return moduleName;
  }

  private static void deleteDir(Path dir) throws IOException
  {
    try (DirectoryStream<Path> s = Files.newDirectoryStream(dir))
    {
      for (Path p : s)
      {
        if (Files.isDirectory(p))
        {
          deleteDir(p);
        }
        Files.delete(p);
      }
    }
  }
}
