package lh.jigsaw.plugin;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugin.CompilationFailureException;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.compiler.util.scan.SimpleSourceInclusionScanner;
import org.codehaus.plexus.compiler.util.scan.SourceInclusionScanner;
import org.codehaus.plexus.compiler.util.scan.StaleSourceScanner;

/**
 * Jigsaw adjustments to the compiler mojo
 *
 * @since 2.0
 * @goal lh-compile
 * @phase compile
 * @threadSafe
 * @requiresDependencyResolution compile
 */
public class CompilerMojo extends AbstractCompilerMojo
{

  /**
   * @parameter expression="${project}"
   * @required
   * @readonly
   */
  private MavenProject project;

  /**
   * The source directories containing the sources to be compiled.
   *
   * @parameter default-value="${project.compileSourceRoots}"
   * @required
   * @readonly
   */
  private List<String> compileSourceRoots;

  /**
   * <p>
   * Specify where to place generated source files created by annotation processing.
   * Only applies to JDK 1.6+
   * </p>
   * @parameter default-value="${project.build.directory}/generated-sources/annotations"
   * @since 2.2
   */
  private File generatedSourcesDirectory;

  /**
   * Project classpath.
   *
   * @parameter default-value="${project.compileClasspathElements}"
   * @required
   * @readonly
   */
  private List<String> classpathElements;

  /**
   * The directory for compiled classes.
   *
   * @parameter default-value="${project.build.outputDirectory}"
   * @required
   * @readonly
   */
  private File outputDirectory;

  /**
   * Project artifacts.
   *
   * @parameter default-value="${project.artifact}"
   * @required
   * @readonly
   * @todo this is an export variable, really
   */
  private Artifact projectArtifact;

  /**
   * A list of inclusion filters for the compiler.
   *
   * @parameter
   */
  private Set<String> includes = new HashSet<>();

  /**
   * A list of exclusion filters for the compiler.
   *
   * @parameter
   */
  private Set<String> excludes = new HashSet<>();

  /**
   * library directory
   *
   * @parameter default-value="${project.build.directory}/library"
   * @required
   */
  private File libraryDirectory;

  @Override
  protected List<String> getCompileSourceRoots()
  {
    return compileSourceRoots;
  }

  @Override
  protected List<String> getClasspathElements()
  {
    return classpathElements;
  }

  @Override
  protected File getOutputDirectory()
  {
    return outputDirectory;
  }

  private void lhJigsawCompilerAdjustments()
  {
    compilerArguments = new HashMap<>(2);

    compilerArguments.put("modulepath", outputDirectory.getAbsolutePath());
    compilerArguments.put("L", libraryDirectory.getAbsolutePath());
  }

  @Override
  public void execute() throws MojoExecutionException, CompilationFailureException
  {
    lhJigsawCompilerAdjustments();

    super.execute();

    projectArtifact.setFile(outputDirectory);
  }

  @Override
  protected SourceInclusionScanner getSourceInclusionScanner(int staleMillis)
  {
    SourceInclusionScanner scanner;

    if (includes.isEmpty() && excludes.isEmpty())
    {
      scanner = new StaleSourceScanner(staleMillis);
    }
    else
    {
      if (includes.isEmpty())
      {
        includes.add("**/*.java");
      }
      scanner = new StaleSourceScanner(staleMillis, includes, excludes);
    }

    return scanner;
  }

  @Override
  protected SourceInclusionScanner getSourceInclusionScanner(String inputFileEnding)
  {
    SourceInclusionScanner scanner;

    if (includes.isEmpty() && excludes.isEmpty())
    {
      includes = Collections.singleton("**/*." + inputFileEnding);
      scanner = new SimpleSourceInclusionScanner(includes, Collections.EMPTY_SET);
    }
    else
    {
      if (includes.isEmpty())
      {
        includes.add("**/*." + inputFileEnding);
      }
      scanner = new SimpleSourceInclusionScanner(includes, excludes);
    }

    return scanner;
  }

  @Override
  protected String getSource()
  {
    return source;
  }

  @Override
  protected String getTarget()
  {
    return target;
  }

  @Override
  protected String getCompilerArgument()
  {
    return compilerArgument;
  }

  @Override
  protected Map<String, String> getCompilerArguments()
  {
    return compilerArguments;
  }

  @Override
  protected File getGeneratedSourcesDirectory()
  {
    return generatedSourcesDirectory;
  }
}
