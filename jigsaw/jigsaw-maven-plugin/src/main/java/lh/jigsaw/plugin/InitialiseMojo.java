package lh.jigsaw.plugin;

import java.io.File;
import java.io.IOException;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.openjdk.jigsaw.SimpleLibrary;

/**
 * Initialise the library if it doesn't exist, will fail the build if the directory is not a valid library<br>
 * It will use ${project.build.directory}/library as the default location for the project library, the JDK library
 * as its parent.
 *
 * @requiresProject
 * @goal initialise
 * @requiresDependencyResolution runtime
 * @phase initialize
 *
 * @author ludovic
 */
public class InitialiseMojo extends AbstractMojo
{
  /**
    * library directory
    *
    * @parameter default-value="${project.build.directory}/library"
    * @required
    */
  private File libraryDirectory; 

  @Override
  public void execute() throws MojoExecutionException, MojoFailureException
  {
    getLog().info("InitialiseMojo");
    try
    {     
      if (libraryDirectory.exists())
      {
        SimpleLibrary.open(libraryDirectory);
      }
      else
      {
        File homeLibrary = new File(System.getProperty("java.home"),
                                    "lib/modules");
        SimpleLibrary.create(libraryDirectory, homeLibrary);
      }
     
    }
    catch (IOException ex)
    {
      throw new MojoExecutionException("Library '" + libraryDirectory + "' is not valid.", ex);
    }
  } 
}
