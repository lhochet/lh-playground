package lh.jigsaw.plugin;

import java.io.File;
import java.io.IOException;
import java.lang.module.ModuleInfo;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.openjdk.jigsaw.JigsawModuleSystem;

/**
 * Runs the project's module from the library.
 *
 * @requiresProject
 * @goal run
 * @requiresDependencyResolution runtime
 *
 * @author ludovic
 */
public class RunMojo extends AbstractMojo
{

  /**
   * library directory
   *
   * @parameter default-value="${project.build.directory}/library"
   * @required
   */
  private File libraryDirectory;

  /**
   * Classes directory.
   *
   * @parameter expression="${project.build.outputDirectory}"
   * @required
   * @readonly
   */
  private File classesDir;

  /**
   * Module name
   *
   * @parameter expression="${moduleName}""
   * @readonly
   */
  private String moduleName;

  /**
   * vm args
   * @parameter default-value="${vmargs}"
   */
  String vmargs;

  /**
   * args
   * @parameter default-value="${args}"
   */
  String args;

  @Override
  public void execute() throws MojoExecutionException, MojoFailureException
  {
    getLog().info("RunMojo");
    try
    {
      List<String> cmdline = new ArrayList<>();

      Path javaCmdDirPath = Paths.get(System.getProperty("java.home"));
      Path javaCmdPath = javaCmdDirPath.resolve("bin/java");
      cmdline.add(javaCmdPath.toString());

      cmdline.add("-L");
      cmdline.add(libraryDirectory.toString());

      cmdline.addAll(split(vmargs));

      cmdline.add("-m");
      cmdline.add(getModuleName());

      cmdline.addAll(split(args));

      ProcessBuilder pb = new ProcessBuilder(cmdline);
      System.out.println("cmd: " + pb.command());

      pb = pb.inheritIO();
      Process p = pb.start();
      int ret = p.waitFor();
      System.out.println("ret = " + ret);
    }
    catch (Exception ex)
    {
      getLog().error(ex);
    }

  }

  private String getModuleName()
  {
    if (moduleName == null)
    {
      try
      {
        Path path = classesDir.toPath().resolve("module-info.class");
        byte[] bytes = java.nio.file.Files.readAllBytes(path);
        JigsawModuleSystem jms = JigsawModuleSystem.instance();
        ModuleInfo mif = jms.parseModuleInfo(bytes);
        moduleName = mif.id().name();
        System.out.println("name = " + moduleName);

      }
      catch (IOException ex)
      {
        getLog().error(ex);
      }
    }
    return moduleName;
  }

  private List<String> split(String arg)
  {
    if (arg == null)
    {
      return Collections.emptyList();
    }

    String[] elts = arg.split(" ");
    return Arrays.asList(elts);
  }
}
