package lh.jigsaw.plugin;

import java.io.File;
import java.io.IOException;
import java.lang.module.ModuleInfo;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.openjdk.jigsaw.JigsawModuleSystem;
import org.openjdk.jigsaw.cli.Librarian;

/**
 * Installs the jmod file into the library.<br>
 * Remove the module from that library if it already exist before adding the new content.
 *
 * @requiresProject
 * @goal jmod
 * @requiresDependencyResolution runtime
 * @phase install
 *
 * @author ludovic
 */
public class JModMojo extends AbstractMojo
{

  /**
   * library directory
   *
   * @parameter default-value="${project.build.directory}/library"
   * @required
   */
  private File libraryDirectory;

  /**
   * Classes directory.
   *
   * @parameter expression="${project.build.outputDirectory}"
   * @required
   * @readonly
   */
  private File classesDir;

  /**
   * module dir
   *
   * @parameter expression="${project.build.directory}/module/"
   * @required
   * @readonly
   */
  private File moduleDir;

  /**
   * Module name
   *
   * @parameter expression="${moduleName}""
   * @readonly
   */
  private String moduleName;

  @Override
  public void execute() throws MojoExecutionException, MojoFailureException
  {
    getLog().info("JModMojo");
    try
    {
      File[] files = moduleDir.listFiles();
      if (files == null)
      {
        throw new MojoExecutionException("no files in <target>/module directory");
      }
      File moduleFile = null;
      for (File f : files)
      {
        if (f.getName().endsWith("jmod"))
        {
          moduleFile = f;
          break;
        }
      }
      if (moduleFile == null)
      {
        throw new MojoExecutionException("no jmod file in <target>/module directory");
      }

      // allow to update the module by removing it, may have to reindex?
      ModuleId moduleId = getModuleId();
      Path installedModule = libraryDirectory.toPath().resolve(moduleId.name()).resolve(moduleId.version().toString());
      if (Files.exists(installedModule))
      {
        String[] params = new String[]
        {
          "-L",
          libraryDirectory.getAbsolutePath(),
          "remove",
          "-f",
          moduleId.toString()
        };

        getLog().info(Arrays.toString(params));

        Librarian.run(params);

      }

      String[] params = new String[]
      {
        "-L",
        libraryDirectory.getAbsolutePath(),
        "install",
        moduleFile.getAbsolutePath()
      };
     
      getLog().info(Arrays.toString(params));
     
      Librarian.run(params);
    }
    catch (MojoExecutionException ex)
    {
      throw ex;
    }
    catch (Exception ex)
    {
      throw new MojoExecutionException("Could not install '" + moduleName + "' into the '" + libraryDirectory + "' library.", ex);
    }

  }

  private String getModuleName() throws MojoExecutionException
  {
    if (moduleName == null)
    {
      try
      {
        Path path = classesDir.toPath().resolve("module-info.class");
        byte[] bytes = java.nio.file.Files.readAllBytes(path);
        JigsawModuleSystem jms = JigsawModuleSystem.instance();
        ModuleInfo mif = jms.parseModuleInfo(bytes);
        moduleName = mif.id().name();
        getLog().info("module name read from module-info = " + moduleName);

      }
      catch (IOException ex)
      {
        throw new MojoExecutionException("Could not read the module name, check that module-info.class exist.", ex);
      }
    }
    return moduleName;
  }

  private static void deleteDir(Path dir) throws IOException
  {
    try (DirectoryStream<Path> s = Files.newDirectoryStream(dir))
    {
      for (Path p : s)
      {
        if (Files.isDirectory(p))
        {
          deleteDir(p);
        }
        Files.delete(p);
      }
    }
  }
}
