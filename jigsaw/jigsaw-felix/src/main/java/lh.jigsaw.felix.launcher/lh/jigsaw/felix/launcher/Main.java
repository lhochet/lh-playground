package lh.jigsaw.felix.launcher;

import java.io.File;
import java.io.IOException;
import java.lang.module.ModuleId;
import java.lang.module.ModuleInfo;
import java.lang.module.ModuleView;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;
import org.openjdk.jigsaw.SimpleLibrary;

/**
 *
 * @author ludovic
 */
public class Main
{
  public static void main(final String[] args)
  { 
    try
    {     
      // Extract information from Jigsaw
      //
      
      final String libraryPath = System.getProperty("sun.java.launcher.module.library");
      
      final SimpleLibrary library = SimpleLibrary.open(new File(libraryPath));
      
      final StringBuilder packages = new StringBuilder();
      final List<URL> urls = new ArrayList<>(); // 'classpath' for launchin Felix
      
      for (ModuleId id:  library.listModuleIds())
      {
        if (isJreModule(id.name()))
        {
          urls.add(library.classPath(id).toURI().toURL());
          
          final ModuleInfo info = library.readModuleInfo(id);
          appendExportedPackages(info, packages);
        }
      }
      
      packages.deleteCharAt(packages.length()-1); 
      
      final File userDir = new File(System.getProperty("user.dir"));
      
      // Additional "classpath" (Felix's jar is added there)
      //      
      addNonOsgiJars(userDir, urls);
      
      // Initialise and start OSGi
      //      
      startOsgi(userDir, urls, packages.toString());
            
    }
    catch (IOException | ClassNotFoundException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException | IllegalAccessException ex)
    {
      throw new RuntimeException("error initialising", ex);
    }
    
    System.out.println("started.");
    
  }

  private static boolean isJreModule(final String moduleName)
  {
    return moduleName.startsWith("java") || moduleName.startsWith("javax");
  }
  
  private static void appendExportedPackages(final ModuleInfo info, final StringBuilder packages)
  {
    final ModuleView view = info.defaultView();
    for (String exportedPackage: view.exports())
    {
      if (!isJavaPackage(exportedPackage))
      {
        packages.append(exportedPackage).append(';');
      }
    }
  }  
  
  private static boolean isJavaPackage(final String packageName)
  {
    // java.* are always available to bundles
    return packageName.startsWith("java.");
  }
  
  private static void addNonOsgiJars(final File userDir, final List<URL> urls) throws MalformedURLException
  {    
    final File bundlesDir = new File(userDir, "nonbundles");
    for (File file : bundlesDir.listFiles())
    {
      urls.add(file.toURI().toURL());
    }
  }
  
  // As Jigsaw currently identificate itself as Java 1.8, and as Felix already has Java 1.8 package,
  // override the full system packages, including Felix's own
  private static final String FELIX_SYSTEM_PACKAGES = 
    "org.osgi.framework; version=1.7.0," +
    " org.osgi.framework.hooks.bundle; version=1.1.0," +
    " org.osgi.framework.hooks.resolver; version=1.0.0," +
    " org.osgi.framework.hooks.service; version=1.1.0," +
    " org.osgi.framework.hooks.weaving; version=1.0.0," +
    " org.osgi.framework.launch; version=1.1.0," +
    " org.osgi.framework.namespace; version=1.0.0," +
    " org.osgi.framework.startlevel; version=1.0.0," +
    " org.osgi.framework.wiring; version=1.1.0," +
    " org.osgi.resource; version=1.0.0," +
    " org.osgi.service.packageadmin; version=1.2.0," +
    " org.osgi.service.startlevel; version=1.1.0," +
    " org.osgi.service.url; version=1.0.0," +
    " org.osgi.util.tracker; version=1.5.1 ";
  
  private static Map<String, Object> getFrameworkProperties(final String packages)
  {
    final Map config = new HashMap<>();
    config.put("org.osgi.framework.system.packages", FELIX_SYSTEM_PACKAGES + "," + packages);
//    config.put("org.osgi.framework.system.packages.extra", packages);
    return config;
  }
  
  
  // we use reflection to not depend on OSGi core (and to not make a module of it)
  
  private static final Class<?>[] NO_ARG_SIG = null;
  private static final Object[] NO_ARG_INV = null;
  
  private static void startOsgi(final File userDir, final List<URL> urls, final String packages) throws ClassNotFoundException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, IllegalAccessException
  {      
      final URLClassLoader cl = getClassLoaderFor(urls);
      
      final Class<?> bundleClass = cl.loadClass("org.osgi.framework.Bundle");      
      final Class<?> bundleContextClass = cl.loadClass("org.osgi.framework.BundleContext");      
      final Class<?> frameworkFactoryClass = cl.loadClass("org.osgi.framework.launch.FrameworkFactory");
      
      final Map<String, Object> properties = getFrameworkProperties(packages);
      
      final Object framework = getFrameworkFor(frameworkFactoryClass, cl, properties);      
      final Class<?> frameworkClass = framework.getClass();
      
      initialiseFramework(frameworkClass, framework);
      
      final Object bundleContext =  getBundleContextFor(frameworkClass, framework);
      
      final List<Object> installedBundles = new ArrayList<>();      
      installBundles(installedBundles, userDir, bundleContextClass, bundleContext);       
      startBundles(installedBundles, bundleClass);
      
      startFramework(frameworkClass, framework);
      
  }

  private static URLClassLoader getClassLoaderFor(final List<URL> urls)
  {
    final URL[] urlsArray = urls.toArray(new URL[urls.size()]);
    
    return new URLClassLoader(urlsArray);
  }

  private static Object getFrameworkFor(final Class<?> frameworkFactoryClass, final URLClassLoader cl, final Map<String, Object> properties) throws IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, IllegalAccessException
  {
    final ServiceLoader<?> sl = ServiceLoader.load(frameworkFactoryClass, cl);
    final Object factory = sl.iterator().next();
    
    final Method newFramework = factory.getClass().getMethod("newFramework", Map.class);
    return newFramework.invoke(factory, properties);
  }

  private static Object getBundleContextFor(final Class<?> frameworkClass, final Object framework) throws NoSuchMethodException, IllegalAccessException, SecurityException, IllegalArgumentException, InvocationTargetException
  {
    final Method getBundleContext = frameworkClass.getDeclaredMethod("getBundleContext", NO_ARG_SIG);
    final Object bundleContext =  getBundleContext.invoke(framework, NO_ARG_INV);
    return bundleContext;
  }

  private static void initialiseFramework(final Class<?> frameworkClass, final Object framework) throws InvocationTargetException, SecurityException, NoSuchMethodException, IllegalAccessException, IllegalArgumentException
  {
    final Method init = frameworkClass.getDeclaredMethod("init", NO_ARG_SIG);
    init.invoke(framework, NO_ARG_INV);
  }
  
  private static void installBundles(final List<Object> installedBundles, final File userDir, final Class<?> bundleContextClass, final Object bundleContext) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException
  {    
    final Method installBundle = bundleContextClass.getDeclaredMethod("installBundle", String.class);
    
    final File bundlesDir = new File(userDir, "bundles");
    for (File file : bundlesDir.listFiles())
    {
      final String name = file.toURI().toString();
      final Object bundle = installBundle.invoke(bundleContext, name);
      installedBundles.add(bundle);
    }
  }

  private static void startBundles(final List<Object> installedBundles, final Class<?> bundleClass) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException
  {   
    final Method start = bundleClass.getDeclaredMethod("start", NO_ARG_SIG);
    
    for (Object bundle: installedBundles)
    {
      start.invoke(bundle, NO_ARG_INV);
    }
  }

  private static void startFramework(final Class<?> frameworkClass, final Object framework) throws IllegalAccessException, SecurityException, NoSuchMethodException, InvocationTargetException, IllegalArgumentException
  {
    final Method start = frameworkClass.getDeclaredMethod("start",  NO_ARG_SIG);
    start.invoke(framework, NO_ARG_INV);
  }
}
