package lh.jigsaw.hk2.adapter;

import com.sun.enterprise.module.ModuleDefinition;
import com.sun.enterprise.module.ModuleDependency;
import com.sun.enterprise.module.ModuleMetadata;
import com.sun.enterprise.module.common_impl.ByteArrayInhabitantsDescriptor;
import com.sun.hk2.component.InhabitantsFile;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.module.ModuleId;
import java.net.URI;
import java.util.Enumeration;
import java.util.jar.Manifest;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import org.openjdk.jigsaw.SimpleLibrary;

/**
 *
 * @author ludovic
 */
public class JigsawModuleDefinition implements ModuleDefinition
{
  private final SimpleLibrary library;
  private final ModuleId id;
  
  private final ModuleMetadata metadata = new ModuleMetadata();
  
  JigsawModuleDefinition(final SimpleLibrary library, final ModuleId id)
  {
    this.library = library;
    this.id = id;
    
    extractMetadata();
  }
  
  private void extractMetadata()
  {
    try
    {
      File cp = library.classPath(id);
      
      //-- read the metadata from the classes.zip
      ZipFile zip = new ZipFile(cp);
      
      ZipEntry entry = zip.getEntry(InhabitantsFile.PATH + "/default");
      if (entry != null)
      {
        InputStream src = zip.getInputStream(entry);
        ByteArrayOutputStream dest = new ByteArrayOutputStream((int)entry.getSize()); // sz shouldn't exceed int sz
        copy(src, dest);
        metadata.addHabitat("default", new ByteArrayInhabitantsDescriptor("default", dest.toByteArray()));
      }
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
  }
  
  
  ModuleId getModuleId()
  {
    return id;
  }

  @Override
  public String getName()
  {
    return id.name();
  }

  @Override
  public String[] getPublicInterfaces()
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public ModuleDependency[] getDependencies()
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public URI[] getLocations()
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public String getVersion()
  {
    return id.version().toString();
  }

  @Override
  public String getImportPolicyClassName()
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public String getLifecyclePolicyClassName()
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public Manifest getManifest()
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public ModuleMetadata getMetadata()
  {
    return metadata;
  }
  
  
  private static void copy(final InputStream src, final OutputStream dest) throws IOException
  {
    final int sz = 512;
    byte[] buf = new byte[sz];
    int nb;
    while ((nb = src.read(buf)) > -1)
    {
      dest.write(buf, 0, nb);
    }
  }
  
}
