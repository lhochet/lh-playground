package lh.jigsaw.hk2.adapter;

import com.sun.enterprise.module.ModuleDefinition;
import com.sun.enterprise.module.ModulesRegistry;
import com.sun.enterprise.module.common_impl.AbstractFactory;
import com.sun.enterprise.module.common_impl.LogHelper;
import com.sun.enterprise.module.common_impl.ModuleId;
import java.io.File;
import java.io.IOException;
import org.openjdk.jigsaw.SimpleLibrary;

/**
 *
 * @author ludovic
 */
public class JigsawFactory extends AbstractFactory
{

  public synchronized static void initialize()
  {
    if (Instance != null)
    {
      LogHelper.getDefaultLogger().fine("Singleton already initialized as " + getInstance());
    }
    Instance = new JigsawFactory();
  }

  @Override
  public ModulesRegistry createModulesRegistry()
  {    
    String libraryPath = System.getProperty("sun.java.launcher.module.library");
    
    try
    {
      SimpleLibrary library = SimpleLibrary.open(new File(libraryPath));
      
      return new JigsawModulesRegistry(library);
    }
    catch (IOException ex)
    {
      throw new RuntimeException("could not create library", ex);
    }
  }

  @Override
  public ModuleId createModuleId(String name, String version)
  {
    return new ModuleId(name, version);
  }

  @Override
  public ModuleId createModuleId(ModuleDefinition md)
  {
    return new ModuleId(md);
  }
}
