package lh.jigsaw.hk2.adapter;

import com.sun.enterprise.module.InhabitantsDescriptor;
import com.sun.enterprise.module.Module;
import com.sun.enterprise.module.ModuleDefinition;
import com.sun.enterprise.module.ModuleLifecycleListener;
import com.sun.enterprise.module.ModulesRegistry;
import com.sun.enterprise.module.ResolveError;
import com.sun.enterprise.module.common_impl.AbstractModulesRegistryImpl;
import com.sun.hk2.component.Holder;
import com.sun.hk2.component.InhabitantsParser;
import java.io.IOException;
import java.lang.module.ModuleId;
import java.lang.module.ModuleInfo;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import org.openjdk.jigsaw.BaseContext;
import org.openjdk.jigsaw.Configuration;
import org.openjdk.jigsaw.ConfigurationException;
import org.openjdk.jigsaw.Configurator;
import org.openjdk.jigsaw.Context;
import org.openjdk.jigsaw.LoaderPool;
import org.openjdk.jigsaw.SimpleLibrary;

/**
 *
 * @author ludovic
 */
public class JigsawModulesRegistry extends AbstractModulesRegistryImpl implements ModulesRegistry
{
  private final SimpleLibrary library;
  private final LoaderPool pool;
  private final Method mfindLoader;
  private final Configuration<Context> config;
  private final Method mAddContext;

  
  JigsawModulesRegistry(final SimpleLibrary library)
  {
    super(null);
    this.library = library;
    addRepository(new JigsawRepository(library));
    
    //< init pool
    // exposes Jigsaw's shared loader's pool so that it can be used to obtain 
    // a class loader for a module
    // also exposes the Configuration<Context> for the same reason
    ClassLoader jigsawloader = getClass().getClassLoader();
    Class<?> clazz = jigsawloader.getClass();

    try
    {
      Field fpool = clazz.getDeclaredField("pool");
      fpool.setAccessible(true);
      Object o = fpool.get(jigsawloader);
      pool = (LoaderPool)o;

      mfindLoader = LoaderPool.class.getDeclaredMethod("findLoader", Context.class);
      mfindLoader.setAccessible(true);
      
      
      Method mconfig = LoaderPool.class.getDeclaredMethod("config", new Class<?>[]{});
      mconfig.setAccessible(true);
      config = (Configuration<Context>)mconfig.invoke(pool, new Object[]{});
            
      mAddContext = config.getClass().getDeclaredMethod("add", BaseContext.class);
      mAddContext.setAccessible(true);
      
    }
    catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException | NoSuchMethodException | InvocationTargetException ex)
    {
      throw new RuntimeException(ex);
    }
    //> init pool
    
    dumpState(System.out);
  }

  @Override
  public void parseInhabitants(Module module, String name, InhabitantsParser inhabitantsParser) throws IOException
  {
    JigsawModuleDefinition jmd = (JigsawModuleDefinition)module.getModuleDefinition();
    ModuleId mid = jmd.getModuleId();
    
    // get a ClassLoader for the module which can later be used by HK2
    final ClassLoader loader = loadModule(mid);
         
    Holder<ClassLoader> holder = new Holder<ClassLoader>() 
    {
      public ClassLoader get() 
      {
        return loader;
      }
    };
    
    
    for (InhabitantsDescriptor d : jmd.getMetadata().getHabitats(name))
    {
      inhabitantsParser.parse(d.createScanner(),holder);
    }

  }
      
  private ClassLoader loadModule(ModuleId mid) throws IOException
  {    
    
    try
    {
      // get a context from the module id which is needed to get a class loader
      Context cx = config.findContextForModuleName(mid.name());
      if (cx == null)
      {
        Configuration<Context> cf = Configurator.configure(library, mid.toQuery());
        if (cf == null)
        {
          throw new Error(mid + ": Module not configured");
        }
        cx = cf.getContextForModuleName(mid.name());
        if (cx == null)
        {
          throw new InternalError(mid + ": Cannot find context");
        }

        mAddContext.invoke(config, cx);

      }

      return (ClassLoader) mfindLoader.invoke(pool, cx);
    } 
    catch (IllegalAccessException | InvocationTargetException ex)
    {
      throw new RuntimeException(ex);
    } 
    catch (ConfigurationException ex)
    {
      throw new Error(mid + ": configuartion exception " + ex.getMessage(), ex);
    }
    
  }
  
 
  @Override
  protected Module newModule(ModuleDefinition moduleDef)
  {
    return new JigsawModule(this, moduleDef);
  }

  @Override
  public ModulesRegistry createChild()
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void detachAll()
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void register(ModuleLifecycleListener listener)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void unregister(ModuleLifecycleListener listener)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void shutdown()
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void setParentClassLoader(ClassLoader parent)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public ClassLoader getParentClassLoader()
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public ClassLoader getModulesClassLoader(ClassLoader parent, Collection<ModuleDefinition> defs) throws ResolveError
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public ClassLoader getModulesClassLoader(ClassLoader parent, Collection<ModuleDefinition> defs, URL[] urls) throws ResolveError
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public Module find(Class clazz)
  {
//    throw new UnsupportedOperationException("Not supported yet.");
    
    ModuleId cmid = clazz.getModule().getModuleInfo().id();
    
    for (Module m: modules.values())
    {
      JigsawModuleDefinition jmd = (JigsawModuleDefinition)m.getModuleDefinition();
      if (cmid.equals(jmd.getModuleId()))
      {
        return m;
      }
    }
    return null;
  }
  
  
}
