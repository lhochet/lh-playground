package lh.jigsaw.hk2.adapter;

import com.sun.enterprise.module.Module;
import com.sun.enterprise.module.ModulesRegistry;
import com.sun.enterprise.module.bootstrap.ArgumentManager;
import com.sun.enterprise.module.bootstrap.BootException;
import com.sun.enterprise.module.bootstrap.ModuleStartup;
import com.sun.enterprise.module.bootstrap.StartupContext;
import com.sun.enterprise.module.common_impl.AbstractFactory;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Collection;
import java.util.Iterator;
import org.jvnet.hk2.component.ComponentException;
import org.jvnet.hk2.component.Habitat;
import org.jvnet.hk2.component.Inhabitant;
import org.jvnet.hk2.component.Inhabitants;
import com.sun.enterprise.module.bootstrap.BootException;
import java.util.logging.Logger;
import com.sun.hk2.component.ExistingSingletonInhabitant;
import com.sun.hk2.component.InhabitantsParser;

/**
 * Based on HK2's Main
 * @author ludovic
 */
public class Main
{
  public static void main(final String[] args)
  {
    (new Main()).run(args);
  }
  
  public Main()
  {
    System.out.println("hello new");
    JigsawFactory.initialize();
  }
  
  public void run(final String[] args)
  {
    System.out.println("hello");
    try
    {
      final Main main = this;
      Thread thread = new Thread()
      {
        @Override
        public void run()
        {
          try
          {
            main.start(args);
          }
          catch (BootException ex)
          {
            ex.printStackTrace();
          }
          catch ( RuntimeException ex)
          {
            ex.printStackTrace();
          }
        }
      };
      thread.start();
      thread.join();
    }
    catch (Throwable t)
    {
      t.printStackTrace();
    }
  }
  
  public void start(final String[] args) throws BootException
  {
    
    final ModulesRegistry mr = AbstractFactory.getInstance().createModulesRegistry();
    
    final StartupContext context = new StartupContext(ArgumentManager.argsToMap(args));
    
    final Habitat habitat = createHabitat(mr, context);
    
    // launch
    final ModuleStartup startupCode = findStartupService(mr, habitat, /*null,*/ context);
    startupCode.setStartupContext(context);
    startupCode.start();
  }
  
  private static final String HABITAT_NAME = "default"; 
  
  private Habitat createHabitat(final ModulesRegistry registry, final StartupContext context) throws BootException
  {
    try
    {
      final Habitat habitat = registry.newHabitat();

        habitat.add(new ExistingSingletonInhabitant<StartupContext>(context));
        habitat.add(new ExistingSingletonInhabitant<Logger>(Logger.global));
        // the root registry must be added as other components sometimes inject it
        habitat.add(new ExistingSingletonInhabitant(ModulesRegistry.class, registry));
        final ClassLoader oldCL = AccessController.doPrivileged(new PrivilegedAction<ClassLoader>() {
            @Override
            public ClassLoader run() {
                ClassLoader cl = Thread.currentThread().getContextClassLoader();
                Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
                return cl;
            }
        });

        try {
            registry.createHabitat(HABITAT_NAME, createInhabitantsParser(habitat));
        } finally {
            AccessController.doPrivileged(new PrivilegedAction<Object>() {
                @Override
                public Object run() {
                    Thread.currentThread().setContextClassLoader(oldCL);
                    return null;
                }
            });
        }
        return habitat;
    }
    catch (ComponentException ex)
    {
      throw new BootException(ex);
    }
  }

    /**
     * Creates {@link InhabitantsParser} to fill in {@link Habitat}.
     * Override for customizing the behavior.
     */
    protected InhabitantsParser createInhabitantsParser(Habitat habitat) {
        return new InhabitantsParser(habitat);
    }
  
    /**
     * Return the ModuleStartup service configured to be used to start the system.
     * @param registry
     * @param habitat
     * @param mainModuleName
     * @param context
     * @return
     * @throws BootException
     */
    public ModuleStartup findStartupService(ModulesRegistry registry, Habitat habitat, /*String mainModuleName, */StartupContext context) throws BootException {
        ModuleStartup startupCode=null;
        final Module mainModule;
/*
        if(mainModuleName!=null) {
            // instantiate the main module, this is the entry point of the application
            // code. it is supposed to have 1 ModuleStartup implementation.
            Collection<Module> modules = registry.getModules(mainModuleName);
            if (modules.size() != 1) {
                if(registry.getModules().isEmpty())
                    throw new BootException("Registry has no module at all");
                else
                    throw new BootException("Cannot find main module " + mainModuleName+" : no such module");
            }
            mainModule = modules.iterator().next();
            String targetClassName = findModuleStartupClassName(mainModule,context.getPlatformMainServiceName(), HABITAT_NAME);
            if (targetClassName==null) {
                throw new BootException("Cannot find a ModuleStartup implementation in the META-INF/services/com.sun.enterprise.v3.ModuleStartup file, aborting");
            }

            Class<? extends ModuleStartup> targetClass=null;
            final ClassLoader currentCL =
                AccessController.doPrivileged(new PrivilegedAction<ClassLoader>() {
                    @Override
                    public ClassLoader run() {
                        ClassLoader cl = Thread.currentThread().getContextClassLoader();
                        Thread.currentThread().setContextClassLoader(mainModule.getClassLoader());
                        return cl;
                    }
                });
            try {
                ClassLoader moduleClassLoader = AccessController.doPrivileged(new PrivilegedAction<ClassLoader>() {
                    @Override
                    public ClassLoader run() {
                        return mainModule.getClassLoader();
                    }
                });
                targetClass = moduleClassLoader.loadClass(targetClassName).asSubclass(ModuleStartup.class);
                startupCode = habitat.getComponent(targetClass);
            } catch (ClassNotFoundException e) {
                throw new BootException("Unable to load component of type " + targetClassName,e);
            } catch (ComponentException e) {
                throw new BootException("Unable to load component of type " + targetClassName,e);
            } finally {
                AccessController.doPrivileged(new PrivilegedAction<Object>() {
                    @Override
                    public Object run() {
                        Thread.currentThread().setContextClassLoader(currentCL);
                        return null;
                    }
                });
            }
        } else {
        */
            Collection<Inhabitant<ModuleStartup>> startups = habitat.getInhabitantsByContract(ModuleStartup.class);

            if(startups.isEmpty())
                throw new BootException("No module has a ModuleStartup implementation");
            if(startups.size()>1) {
                // maybe the user specified a main
                String mainServiceName = context.getPlatformMainServiceName();
                for (Inhabitant<? extends ModuleStartup> startup : startups) {
                    Collection<String> regNames = Inhabitants.getNamesFor(startup, ModuleStartup.class.getName());
                    if (regNames.isEmpty() && mainServiceName==null) {
                        startupCode = startup.get();
                    } else {
                        for (String regName : regNames) {
                            if (regName.equals(mainServiceName)) {
                                startupCode = startup.get();
                            }
                        }
                    }

                }
                if (startupCode==null) {
                    if (mainServiceName==null) {
                        Iterator<Inhabitant<ModuleStartup>> itr = startups.iterator();
                        ModuleStartup a = itr.next().get();
                        ModuleStartup b = itr.next().get();
                        Module am = registry.find(a.getClass());
                        Module bm = registry.find(b.getClass());
                        throw new BootException(String.format("Multiple ModuleStartup found: %s from %s and %s from %s",a,am,b,bm));
                    } else {
                        throw new BootException(String.format("Cannot find %s ModuleStartup", mainServiceName));
                    }
                }
            } else {
                startupCode = startups.iterator().next().get();
            }
            mainModule = registry.find(startupCode.getClass());
        /*}*/

        habitat.addIndex(Inhabitants.create(startupCode),
                ModuleStartup.class.getName(), habitat.DEFAULT_NAME);
        mainModule.setSticky(true);
        return startupCode;
    }
}
