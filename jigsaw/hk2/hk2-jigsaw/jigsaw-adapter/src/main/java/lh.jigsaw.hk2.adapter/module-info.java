module lh.jigsaw.hk2.adapter @ 0.1
{
  requires jdk.base;
  requires jdk.desktop;

  requires jdk.logging;

  requires lh.jigsaw.hk2.core @ 0.1;
  requires lh.jigsaw.hk2.autodepends @ 0.1;

  class lh.jigsaw.hk2.adapter.Main;
}
