package lh.jigsaw.hk2.adapter;

import com.sun.enterprise.module.ModuleDefinition;
import com.sun.enterprise.module.Repository;
import com.sun.enterprise.module.RepositoryChangeListener;
import java.io.IOException;
import java.lang.module.ModuleId;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.openjdk.jigsaw.SimpleLibrary;

/**
 *
 * @author ludovic
 */
public class JigsawRepository implements Repository
{
  private final SimpleLibrary library;
  
  JigsawRepository(final SimpleLibrary library)
  {
    this.library = library;
  }

  @Override
  public String getName()
  {
    return "JigsawSimpleLibrary";
  }

  @Override
  public URI getLocation()
  {
    return library.location();
  }

  @Override
  public ModuleDefinition find(String name, String version)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public List<ModuleDefinition> findAll()
  {
    try
    {
      List<ModuleId> ids = library.listLocalModuleIds();
      List<ModuleDefinition> ret = new ArrayList<>(ids.size());
      for (ModuleId id: ids)
      {
        ret.add(new JigsawModuleDefinition(library, id));
      }
      return ret;
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
      return Collections.emptyList();
    }
  }

  @Override
  public List<ModuleDefinition> findAll(String name)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void initialize() throws IOException
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void shutdown() throws IOException
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public List<URI> getJarLocations()
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public boolean addListener(RepositoryChangeListener listener)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public boolean removeListener(RepositoryChangeListener listener)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

 
  
}
