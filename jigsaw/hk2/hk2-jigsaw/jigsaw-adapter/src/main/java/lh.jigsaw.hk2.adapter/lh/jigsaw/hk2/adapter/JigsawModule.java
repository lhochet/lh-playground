package lh.jigsaw.hk2.adapter;

import com.sun.enterprise.module.Module;
import com.sun.enterprise.module.ModuleChangeListener;
import com.sun.enterprise.module.ModuleDefinition;
import com.sun.enterprise.module.ModuleDependency;
import com.sun.enterprise.module.ModuleMetadata;
import com.sun.enterprise.module.ModuleState;
import com.sun.enterprise.module.ModulesRegistry;
import com.sun.enterprise.module.ResolveError;
import com.sun.hk2.component.Holder;
import com.sun.hk2.component.InhabitantsParser;
import java.io.IOException;
import java.io.PrintStream;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ludovic
 */
public class JigsawModule implements Module
{
  
  final ModulesRegistry registry;
  final ModuleDefinition moduleDefinition;
  
  
  
  JigsawModule(final ModulesRegistry registry, final ModuleDefinition moduleDefinition)
  {
    this.registry = registry;
    this.moduleDefinition = moduleDefinition;
  }

  @Override
  public ModuleDefinition getModuleDefinition()
  {
    return moduleDefinition;
  }

  @Override
  public String getName()
  {
    return moduleDefinition.getName();
  }

  @Override
  public ModulesRegistry getRegistry()
  {
    return registry;
  }

  @Override
  public ModuleState getState()
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void resolve() throws ResolveError
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void start() throws ResolveError
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public boolean stop()
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void detach()
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void refresh()
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public ModuleMetadata getMetadata()
  {
    return moduleDefinition.getMetadata();
  }

  @Override
  public void addListener(ModuleChangeListener listener)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void removeListener(ModuleChangeListener listener)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public ClassLoader getClassLoader()
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public List<Module> getImports()
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void addImport(Module module)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public Module addImport(ModuleDependency dependency)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public boolean isShared()
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  
  @Override
  public boolean isSticky()
  {
    return true; // same as OSGi
  }

  @Override
  public void setSticky(boolean sticky)
  {
    // same as OSGi
  }

  @Override
  public <T> Iterable<Class<? extends T>> getProvidersClass(Class<T> serviceClass)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public Iterable<Class> getProvidersClass(String name)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public boolean hasProvider(Class serviceClass)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void dumpState(PrintStream writer)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void uninstall()
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }
    
}
