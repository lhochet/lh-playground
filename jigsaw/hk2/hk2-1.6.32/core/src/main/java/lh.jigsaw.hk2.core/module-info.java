module lh.jigsaw.hk2.core @ 0.1
{
  requires jdk.base;
  requires jdk.desktop;

  requires jdk.logging;

  requires lh.jigsaw.hk2.autodepends @ 0.1;
  //requires optional lh.jigsaw.hk2.config
  //requires lh.jigsaw.hk2.api;

  exports com.sun.enterprise.module;
  exports com.sun.enterprise.module.bootstrap;
  exports com.sun.enterprise.module.common_impl;
  exports com.sun.enterprise.module.single;
  exports com.sun.enterprise.module.impl;

  //class com.sun.enterprise.module.bootstrap.Main;
}
