module lh.jigsaw.hk2.autodepends @ 0.1
{
  requires jdk.base;
  requires jdk.desktop;
  requires jdk.jx.annotations;
  requires jdk.logging;

  //requires optional lh.jigsaw.hk2.config
  requires lh.jigsaw.hk2.api;
  requires lh.jigsaw.hk2.jsr330;

  exports org.jvnet.hk2.component;
  exports org.jvnet.hk2.tracing;
  exports org.jvnet.hk2.component.matcher;
  exports org.glassfish.hk2.classmodel.reflect;
  exports org.glassfish.hk2.classmodel.reflect.util;
  exports org.jvnet.tiger_types;
  exports com.sun.hk2.component;



}
