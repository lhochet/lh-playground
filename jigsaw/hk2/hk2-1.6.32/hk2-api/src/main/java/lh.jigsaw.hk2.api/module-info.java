module lh.jigsaw.hk2.api @ 0.1
{
  requires jdk.base;
  requires jdk.desktop;

  //requires lh.jigsaw.hk2.autodepends @ 0.1;
  //requires optional lh.jigsaw.hk2.config

  //exports com.sun.enterprise.module;
  //exports com.sun.enterprise.module.bootstrap;
  //exports com.sun.enterprise.module.common_impl;
  //exports com.sun.enterprise.module.single;
  //exports com.sun.enterprise.module.impl;
  exports org.glassfish.hk2;
  exports org.glassfish.hk2.inject;
  exports org.glassfish.hk2.scopes;
  exports org.glassfish.hk2.spi;
  exports org.jvnet.hk2.annotations;

  //class com.sun.enterprise.module.bootstrap.Main;
}
