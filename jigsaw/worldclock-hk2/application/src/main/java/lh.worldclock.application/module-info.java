module lh.worldclock.application @ 0.7
{
  requires jdk.base;
  requires jdk.desktop;

  requires lh.jigsaw.hk2.core;
  requires lh.jigsaw.hk2.api;

  requires lh.worldclock.panel;
//  class lh.worldclock.WorldClock;
}
