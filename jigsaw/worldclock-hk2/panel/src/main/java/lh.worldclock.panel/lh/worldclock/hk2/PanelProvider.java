/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lh.worldclock.panel.lh.worldclock.hk2;

import lh.worldclock.core.WorldClockBoard;
import org.jvnet.hk2.annotations.Contract;

/**
 *
 * @author ludovic
 */
@Contract
public interface PanelProvider 
{
  WorldClockBoard getPanel();
}
