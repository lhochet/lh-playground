module lh.worldclock.panel @ 0.7
{
  requires jdk.base;
  requires jdk.desktop;
  exports lh.worldclock.core;
  exports lh.worldclock.hk2;
}
